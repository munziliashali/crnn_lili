#rm -rf expr_bin_nh256_adadelta_nama
#rm -rf lmdb_ktp_nama
#python create_lmdb_nama.py
python crnn_main.py --trainroot lmdb_ktp_nama_train \
--valroot lmdb_ktp_nama_val \
--alphabet "am:ABCDEFGHIJKLMNOPQRSTUVWXYZ-_;+& .,b'/\"" \
--cuda \
--batchSize 128 \
--nh 512 \
--niter 5000 \
--ngpu 2 \
--random_sample \
--displayInterval 24 \
--valInterval 24 \
--saveInterval 10 \
--n_test_disp 20 \
--lr 0.00001 \
--logs logs_bin_nh256_rms_nama_split \
--experiment expr_bin_nh256_rms_nama_split \
#--crnn expr_bin_nh256_rms_nama_split/netCRNN_20_loss6.pth \
#--adadelta \
#--adam \
#--alphabet "am:ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890eglhirpt-;+& .,b'/\"" \
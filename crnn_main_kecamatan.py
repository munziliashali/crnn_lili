from __future__ import print_function
import argparse
import random
import torch
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
from torch.autograd import Variable
import numpy as np
from warpctc_pytorch import CTCLoss
import os
import utils
import dataset
from logger import Logger

import models.crnn_kecamatan as crnn

torch.cuda.set_device(1)
parser = argparse.ArgumentParser()
parser.add_argument('--trainroot', required=True, help='path to dataset')
parser.add_argument('--valroot', required=True, help='path to dataset')
parser.add_argument('--workers', type=int, help='number of data loading workers', default=2)
parser.add_argument('--batchSize', type=int, default=64, help='input batch size')
parser.add_argument('--imgH', type=int, default=32, help='the height of the input image to network')
parser.add_argument('--imgW', type=int, default=100, help='the width of the input image to network')
parser.add_argument('--nh', type=int, default=256, help='size of the lstm hidden state')
parser.add_argument('--niter', type=int, default=25, help='number of epochs to train for')
parser.add_argument('--lr', type=float, default=0.00001, help='learning rate for Critic, default=0.00005')
parser.add_argument('--beta1', type=float, default=0.5, help='beta1 for adam. default=0.5')
parser.add_argument('--cuda', action='store_true', help='enables cuda')
parser.add_argument('--ngpu', type=int, default=2, help='number of GPUs to use')
parser.add_argument('--crnn', default='', help="path to crnn (to continue training)")
parser.add_argument('--alphabet', type=str, default='0123456789abcdefghijklmnopqrstuvwxyz')
parser.add_argument('--experiment', default=None, help='Where to store samples and models')
parser.add_argument('--logs', default=None, help='Where to store logger')
parser.add_argument('--displayInterval', type=int, default=500, help='Interval to be displayed')
parser.add_argument('--n_test_disp', type=int, default=10, help='Number of samples to display when test')
parser.add_argument('--valInterval', type=int, default=500, help='Interval to be displayed')
parser.add_argument('--saveInterval', type=int, default=500, help='Interval to be displayed')
parser.add_argument('--adam', action='store_true', help='Whether to use adam (default is rmsprop)')
parser.add_argument('--adadelta', action='store_true', help='Whether to use adadelta (default is rmsprop)')
parser.add_argument('--keep_ratio', action='store_true', help='whether to keep ratio for image resize')
parser.add_argument('--random_sample', action='store_true', help='whether to sample the dataset with random sampler')
parser.add_argument('--lower', action='store_true', help='whether to use only uppercase characters')
parser.add_argument('--print_iter', action='store_true', help='whether print iterations')
opt = parser.parse_args()

if opt.experiment is None:
    opt.experiment = 'expr'
os.system('mkdir {0}'.format(opt.experiment))

logname = str(opt.experiment) + ".log"
def LogSave(content, filename=logname):
    filename = os.path.join(opt.experiment, filename) 
    print(content)
    with open(filename, "a") as myfile:
        myfile.write(content + "\n")

LogSave(str(opt))
opt.manualSeed = random.randint(1, 10000)  # fix seed
LogSave("Random Seed: " + str(opt.manualSeed))
random.seed(opt.manualSeed)
np.random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)

cudnn.benchmark = True

if torch.cuda.is_available() and not opt.cuda:
    print("WARNING: You have a CUDA device, so you should probably run with --cuda")

train_dataset = dataset.lmdbDataset(root=opt.trainroot)
assert train_dataset
if not opt.random_sample:
    sampler = dataset.randomSequentialSampler(train_dataset, opt.batchSize)
    shuffler = False
    print("Not shuffling")
else:
    sampler = None
    shuffler = True
    print("Shuflling")
train_loader = torch.utils.data.DataLoader(
    train_dataset, batch_size=opt.batchSize,
    shuffle=shuffler, sampler=sampler, drop_last=True,
    num_workers=int(opt.workers),
    collate_fn=dataset.alignCollate(imgH=opt.imgH, imgW=opt.imgW, keep_ratio=opt.keep_ratio))
test_dataset = dataset.lmdbDataset(root=opt.valroot)
    #root=opt.valroot, transform=dataset.resizeNormalize((100, 32)))
#test_dataset = dataset.lmdbDataset(root=opt.valroot, transform=dataset.resizeNormalize((100, 32)))

nclass = len(opt.alphabet) + 1
nc = 1 #number convolution layer

converter = utils.strLabelConverter(opt.alphabet, ignore_case=False)
criterion = CTCLoss()

logger = Logger('./%s' % opt.logs)
# custom weights initialization called on crnn
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

crnn = crnn.CRNN(opt.imgH, nc, nclass, opt.nh)
crnn.apply(weights_init)
if opt.crnn != '':
    LogSave('loading pretrained model from %s' % opt.crnn)
    # original saved file with DataParallel
    state_dict = torch.load(opt.crnn)
    # create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict.items():
        name = k[7:] # remove `module.`
        new_state_dict[name] = v
    # load params
    crnn.load_state_dict(new_state_dict)
    LogSave(str(crnn))

image = torch.FloatTensor(opt.batchSize, 3, opt.imgH, opt.imgH)
text = torch.IntTensor(opt.batchSize * 5)
length = torch.IntTensor(opt.batchSize)

if opt.cuda:
    crnn.cuda()
    #crnn = torch.nn.DataParallel(crnn)
    #crnn = torch.nn.DataParallel(crnn, device_ids=range(opt.ngpu))
    crnn = torch.nn.DataParallel(crnn, device_ids=[1])
    image = image.cuda()
    criterion = criterion.cuda()

image = Variable(image)
text = Variable(text)
length = Variable(length)

# loss averager
loss_avg = utils.averager()

# setup optimizer
if opt.adam:
    optimizer = optim.Adam(crnn.parameters(), lr=opt.lr,
                           betas=(opt.beta1, 0.999))
elif opt.adadelta:
    optimizer = optim.Adadelta(crnn.parameters(), lr=opt.lr)
else:
    optimizer = optim.RMSprop(crnn.parameters(), lr=opt.lr, momentum=0.9)
    #optimizer = optim.SGD(crnn.parameters(), lr=opt.lr, momentum=0.9)


def val(net, val_dataset, criterion, max_iter=100):
    print('Start val')

    for p in crnn.parameters():
        p.requires_grad = False

    net.eval()
    
    data_loader = torch.utils.data.DataLoader(
        val_dataset, shuffle=True, drop_last =True, batch_size=opt.batchSize, num_workers=int(opt.workers), collate_fn=dataset.alignCollate(imgH=opt.imgH, imgW=opt.imgW, keep_ratio=opt.keep_ratio))
    val_iter = iter(data_loader)

    i = 0
    n_correct = 0
    loss_avg = utils.averager()

    max_iter = min(max_iter, len(data_loader))

    for i in range(max_iter):
        data = val_iter.next()
        i += 1
        cpu_images, cpu_texts = data
        batch_size = cpu_images.size(0)
        utils.loadData(image, cpu_images)
        t, l = converter.encode(cpu_texts)
        utils.loadData(text, t)
        utils.loadData(length, l)
        
        preds = crnn(image) #51, 256, 59
        preds_size = Variable(torch.IntTensor([preds.size(0)] * batch_size)) #256
        cost = criterion(preds, text, preds_size, length) / batch_size #1
        loss_avg.add(cost)

        _, preds = preds.max(2) #51, 256
        #_, preds = preds.max(1) # with attention
        
        #print(preds.size())
        preds = preds.squeeze(-2) #51, 256
        #preds = preds.squeeze(1)
        preds = preds.transpose(1, 0).contiguous().view(-1)
        sim_preds = converter.decode(preds.data, preds_size.data, raw=False)
        for pred, target in zip(sim_preds, cpu_texts):
            if opt.lower:
                if pred == target.lower():
                    n_correct +=1
            else:
                if pred == target:
                    n_correct += 1

    raw_preds = converter.decode(preds.data, preds_size.data, raw=True)[:opt.n_test_disp]
    for raw_pred, pred, gt in zip(raw_preds, sim_preds, cpu_texts):
        LogSave('%-20s => %-20s, gt: %-20s' % (raw_pred, pred, gt))
    
    LogSave('Num correct: {} of {}'.format(n_correct, int(max_iter) * int(opt.batchSize)))
    accuracy = n_correct / float(max_iter * opt.batchSize)
    loss_val = loss_avg.val()
    LogSave('Test loss: %f, accuracy: %f' % (loss_avg.val(), accuracy))
    return accuracy, loss_val


def trainBatch(net, criterion, optimizer):
    data = train_iter.next()
    cpu_images, cpu_texts = data
    batch_size = cpu_images.size(0)
    utils.loadData(image, cpu_images)
    t, l = converter.encode(cpu_texts)
    utils.loadData(text, t)
    utils.loadData(length, l)

    preds = crnn(image)
    preds_size = Variable(torch.IntTensor([preds.size(0)] * batch_size))
    cost = criterion(preds, text, preds_size, length) / batch_size
    crnn.zero_grad()
    cost.backward()
    optimizer.step()
    return cost

for epoch in range(opt.niter):
    train_iter = iter(train_loader)
    i = 0
    while i < len(train_loader):
        for p in crnn.parameters():
            p.requires_grad = True
        crnn.train()

        cost = trainBatch(crnn, criterion, optimizer)
        loss_avg.add(cost)
        i += 1
        #print("Run iteration {}".format(i))
        
        if i % opt.displayInterval == 0:
            tr_loss = loss_avg.val()
            LogSave('[%d/%d][%d/%d] Loss: %f' % (epoch, opt.niter, i, len(train_loader), loss_avg.val()))
            loss_avg.reset()

        if i % opt.valInterval == 0:
            #val(crnn, test_dataset, criterion)
            accuracy, loss_val = val(crnn, test_dataset, criterion)
            info = { 'loss': tr_loss, 'accuracy_val': accuracy, 'loss_val': loss_val }
            for tag, value in info.items():
                logger.scalar_summary(tag, value, i+1)
    # do checkpointing
    if epoch % opt.saveInterval == 0:
        torch.save(crnn.state_dict(), '{0}/netCRNN_{1}.pth'.format(opt.experiment, epoch))
        LogSave("Weight File : netCRNN_" + str(epoch) + ".pth saved.")
        
torch.save(crnn.state_dict(), '{0}/netCRNN_{1}.pth'.format(opt.experiment, opt.niter))
LogSave("Weight File : netCRNN_" + str(opt.niter) + ".pth saved.")
#rm -rf expr_bin_nh256_adadelta_nama
#rm -rf lmdb_ktp_nama
#python create_lmdb_nama.py
python crnn_main_alamat.py --trainroot lmdb_ktp_alamat_jalan_train \
--valroot lmdb_ktp_alamat_jalan_val \
--alphabet "lamt:ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_- .,b'/\"" \
--cuda \
--batchSize 128 \
--nh 256 \
--niter 5000 \
--ngpu 2 \
--random_sample \
--displayInterval 24 \
--valInterval 24 \
--saveInterval 20 \
--n_test_disp 20 \
--lr 0.00001 \
--logs logs_bin_nh256_rms_alamat_split \
--experiment expr_bin_nh256_rms_alamat_split \
#--crnn expr_bin_nh256_sgd_alamat/netCRNN_40_acc86.pth \
#--adadelta \
#--adam \
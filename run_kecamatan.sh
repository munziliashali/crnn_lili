python crnn_main_kecamatan.py --trainroot lmdb_ktp_kecamatan_train \
--valroot lmdb_ktp_kecamatan_val \
--alphabet "am:ABCDEFGHIJKLMNOPQRSTUVWXYZectn b'-.,[/\"" \
--cuda \
--batchSize 128 \
--nh 256 \
--niter 5000 \
--ngpu 2 \
--random_sample \
--displayInterval 25 \
--valInterval 25 \
--saveInterval 10 \
--n_test_disp 20 \
--lr 0.00001 \
--logs logs_bin_nh256_rms_kecamatan_split \
--experiment expr_bin_nh256_rms_kecamatan_split \
#--crnn expr_bin_nh256_rms_kecamatan_augmented/netCRNN_180_loss5.pth \
#--adadelta \
#--adam \
#339

import torch.nn as nn
import torch
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torch.nn.parallel import data_parallel
from torch.autograd import Variable

import utils

torch.cuda.set_device(0)


class BidirectionalLSTM(nn.Module):

    def __init__(self, nIn, nHidden, nOut,dropout_p=0.9):
        super(BidirectionalLSTM, self).__init__()

        self.rnn = nn.LSTM(nIn, nHidden, bidirectional=True)
        self.embedding = nn.Linear(nHidden*2, nOut)
        self.dropout = nn.Dropout(dropout_p)



    def forward(self, input):
        recurrent, _ = self.rnn(self.dropout(input))
        #recurrent, _ = self.rnn(input)
        # T is max sequence length
        # b is the batch size
        # print(recurrent.size())
        T, b, h = recurrent.size() # 48, 16, 1024 => T, b constant, h berubah
        t_rec = recurrent.view(T * b, h)
        
        output = self.embedding(t_rec)  # [T * b, nOut]
        #print(output.size())
        output = output.view(T, b, -1)
        #print(output.size())
        return output
    
def gaussian(ins, mean, stddev):
    noise = Variable(ins.data.new(ins.size()).normal_(mean, stddev))
    return ins + noise

class CRNN(nn.Module):

    def __init__(self, imgH, nc, nclass, nh, n_rnn=2):
        super(CRNN, self).__init__()
        assert imgH % 16 == 0, 'imgH has to be a multiple of 16'
        
        ks = [3, 3, 3, 3, 3, 3, 5] #kernel size
        ps = [2, 1, 1, 1, 1, 1, 1] #padding size
        ss = [1, 1, 1, 1, 1, 1, 2] #stride size
        nm = [256, 256, 256, 512, 512, 512, 512] #neuron
        '''
        ks = [3, 3, 3, 3] #kernel size
        ps = [0, 0, 0, 0] #padding size
        ss = [1, 1, 1, 1] #stride size
        nm = [512, 512, 512, 512] #neuron
        '''
        cnn = nn.Sequential()
        
        def convRelu(i, batchNormalization=False):
            nIn = nc if i == 0 else nm[i - 1]
            nOut = nm[i]
            cnn.add_module('conv{0}'.format(i),
                           nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
            if batchNormalization:
                cnn.add_module('batchnorm{0}'.format(i), nn.BatchNorm2d(nOut))
            #if leakyRelu:
            #    cnn.add_module('relu{0}'.format(i),
            #                   nn.LeakyReLU(0.1, inplace=True))
            else:
                cnn.add_module('relu{0}'.format(i), nn.ReLU(True))
        
        convRelu(0, True)
        cnn.add_module('pooling{0}'.format(0), nn.AvgPool2d((2, 2), (2, 1), (0, 1)))
        convRelu(1, True)
        convRelu(2, True)
        cnn.add_module('pooling{0}'.format(1), nn.AvgPool2d((2, 2), (2, 1), (0, 1)))
        convRelu(3, True)
        convRelu(4, True)
        cnn.add_module('pooling{0}'.format(2), nn.AvgPool2d((2, 2), (2, 1), (0, 1)))
        convRelu(5, True)
        convRelu(6, True)
        cnn.add_module('pooling{0}'.format(2), nn.MaxPool2d((2, 2), (2, 1), (0, 1)))
        '''
        convRelu(0, True)
        cnn.add_module('pooling{0}'.format(0), nn.MaxPool2d((2, 2), (2, 1), (0, 1)))
        #cnn.add_module('relu{0}'.format(0), nn.ReLU(True))
        convRelu(1, True)
        #cnn.add_module('relu{0}'.format(1), nn.ReLU(True))
        convRelu(2, True)
        cnn.add_module('pooling{0}'.format(2), nn.MaxPool2d((2, 2), (2, 1), (0, 1)))
        #cnn.add_module('relu{0}'.format(2), nn.ReLU(True))
        convRelu(3, True)
        cnn.add_module('pooling{0}'.format(3), nn.MaxPool2d((2, 2), (2, 1), (0, 1)))
        #cnn.add_module('relu{0}'.format(3), nn.ReLU(True))
        '''
        self.cnn = cnn
        #self.dropout = nn.Dropout(0.5)
        self.rnn = nn.Sequential(
            #BidirectionalLSTM(512, 256, 256),
            BidirectionalLSTM(512, 512, 512),
            BidirectionalLSTM(512, 1024, nclass))

    def forward(self, input):
        # conv features
        conv = self.cnn(input)
        #conv = self.dropout(self.cnn(input))
        #print(conv.size()) #len of char 95
        b, c, h, w = conv.size() #256, 256, 1, 51
        assert h == 1, "the height of conv must be 1"
        conv = conv.squeeze(2)
        conv = conv.permute(2, 0, 1)  # [w, b, c]
        #print(conv.size())
        # rnn features
        output = self.rnn(conv) #51, 256, 59
        #output = data_parallel(self.rnn, conv, self.ngpu)

        return output

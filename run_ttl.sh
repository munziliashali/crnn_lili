#rm -rf expr_bin_nh256_adadelta_nama
#rm -rf lmdb_ktp_nama
#python create_lmdb_nama.py
python crnn_main_ttl.py --trainroot lmdb_ktp_ttl_train \
--valroot lmdb_ktp_ttl_val \
--alphabet "empatglhir:ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890[-;+ ,.&b'/\"" \
--cuda \
--batchSize 32 \
--nh 256 \
--niter 150 \
--ngpu 2 \
--random_sample \
--displayInterval 95 \
--valInterval 95 \
--saveInterval 10 \
--n_test_disp 20 \
--lr 0.000005 \
--logs logs_bin_nh512_sgd_ttl_split \
--experiment expr_bin_nh512_sgd_ttl_split \
#--crnn expr_bin_nh512_sgd_ttl_gaussian_newdata/netCRNN_150_acc84.pth \
#--adam \
#--adadelta \
#--stddev 0.4 \
#--mean 0 \
#--alphabet "am:ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890eglhirpt-;+ ,.&b'/\"" \
#rm -rf expr_bin_nh256_adadelta_nama
#rm -rf lmdb_ktp_nama
#python create_lmdb_nama.py
python crnn_main_pekerjaan.py --trainroot lmdb_ktp_pekerjaan_train \
--valroot lmdb_ktp_pekerjaan_val \
--alphabet "ekrjan:ABCDEFGHIJKLMNOPQRSTUVWXYZ()-_;+& .,b'/\"" \
--cuda \
--batchSize 64 \
--nh 256 \
--niter 150 \
--ngpu 2 \
--random_sample \
--displayInterval 57 \
--valInterval 57 \
--saveInterval 1 \
--n_test_disp 20 \
--lr 0.0001 \
--logs logs_bin_nh256_SGD_pekerjaan_split \
--experiment expr_bin_nh256_SGD_pekerjaan_split \
#--crnn expr_bin_nh256_rms_nama_split/netCRNN_20.pth \
#--adadelta \
#--adam \
#--alphabet "am:ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890eglhirpt-;+& .,b'/\"" \
#489
import torch
from torch.autograd import Variable
import utils
import dataset
from PIL import Image

import models.crnn_nama_acc91 as crnn


model_path = 'expr_bin_nh256_rms_nama_newbatch/netCRNN_280.pth'
img_path = 'nama/nama-Copy1.jpg'
alphabet = "am:ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890eglhirpt-;+& .,b'/\""

model = crnn.CRNN(32, 1, 59, 256)
model.eval()
if torch.cuda.is_available():
    model = model.cuda()
print('loading pretrained model from %s' % model_path)
state_dict = torch.load(model_path)
# create new OrderedDict that does not contain `module.`
from collections import OrderedDict
new_state_dict = OrderedDict()
for k, v in state_dict.items():
    name = k[7:] # remove `module.`
    new_state_dict[name] = v
    # load params
model.load_state_dict(new_state_dict)


converter = utils.strLabelConverter(alphabet, False)

transformer = dataset.resizeNormalize((100, 32))
image = Image.open(img_path).convert('L')
image = transformer(image)
if torch.cuda.is_available():
    image = image.cuda()
image = image.view(1, *image.size())
image = Variable(image)


preds = model(image)

_, preds = preds.max(2)
preds = preds.transpose(1, 0).contiguous().view(-1)

preds_size = Variable(torch.IntTensor([preds.size(0)]))
raw_pred = converter.decode(preds.data, preds_size.data, raw=True)
sim_pred = converter.decode(preds.data, preds_size.data, raw=False)
print('%-20s => %-20s' % (raw_pred, sim_pred))
